package com.onebyte.setup.ui.home

import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import com.onebyte.setup.R
import com.onebyte.setup.databinding.ActivityHomeBinding
import com.onebyte.setup.ui.base.BaseActivity
import com.onebyte.setup.ui.base.BaseViewModel
import com.onebyte.setup.utils.ProgressHUD
import com.onebyte.setup.ui.home.fragments.HomeFragment
import com.onebyte.setup.utils.toast
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeActivity : BaseActivity<ActivityHomeBinding, HomeViewModel>() {

    override fun getViewModelClass(): Class<HomeViewModel> = HomeViewModel::class.java

    @Inject
    lateinit var homeFragment: HomeFragment



    override fun layoutId(): Int = R.layout.activity_home

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        setListeners()
    }

    private fun loadFragment(fragment: Fragment, tag: String){
        supportFragmentManager
            .beginTransaction()
            .addToBackStack(null)
            .setCustomAnimations(R.anim.slide_up, R.anim.slide_down)
            .replace(R.id.flHomeContainer, fragment, tag)
            .commit()
    }

    private fun initViews() {
//        customPopups.showAlertPopup("this","test",this,null)
        loadFragment(homeFragment, homeFragment.TAG)
        progressHUD.showLoadingDialog()
    }

    private fun setListeners() {
        toast("lkasdmfl")
    }

    override fun onBackPressed() {
        finish()
    }
}
