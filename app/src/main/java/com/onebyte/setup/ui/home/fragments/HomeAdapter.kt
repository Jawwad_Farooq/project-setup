package com.onebyte.setup.ui.home.fragments

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.onebyte.setup.R
import com.onebyte.setup.databinding.ItemHomeBinding
import com.onebyte.setup.di.assistedFactory.LinearlayoutAI
import javax.inject.Inject

class HomeAdapter @Inject constructor(private val layoutManagerAI: LinearlayoutAI.Factory) : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {
    var list: List<String> = arrayListOf()
    var context: Context? = null

    class ViewHolder(private val binding: ItemHomeBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: String, layoutManagerAI: LinearlayoutAI.Factory, context: Context){
            with(binding){
                name = item
                isParent = true
                val adapter = HomeAdapter2()
                recyclerView.adapter = adapter
                adapter.updateList(arrayListOf("4", "3", "2", "1"))
                recyclerView.layoutManager = layoutManagerAI.create(context)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_home, parent, false))
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder){
            bind(list[position], layoutManagerAI, context!!)
        }
    }


    fun updateList(newList: List<String>) {
        (list as ArrayList).clear()
        (list as ArrayList).addAll(newList)
        notifyItemRangeInserted(0, newList.size)
    }
}