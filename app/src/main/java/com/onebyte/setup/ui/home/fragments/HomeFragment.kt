package com.onebyte.setup.ui.home.fragments

import android.os.Bundle
import android.view.View
import com.onebyte.setup.R
import com.onebyte.setup.databinding.FragmentHomeBinding
import com.onebyte.setup.di.assistedFactory.LinearlayoutAI
import com.onebyte.setup.ui.base.BaseFragment
import com.onebyte.setup.ui.home.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject


@AndroidEntryPoint
class HomeFragment @Inject constructor() : BaseFragment<FragmentHomeBinding, HomeViewModel>() {
    override fun getViewModelClass(): Class<HomeViewModel> = HomeViewModel::class.java

    override val layoutId: Int get() = R.layout.fragment_home

    val TAG = HomeFragment::class.java.simpleName

    @Inject
    lateinit var layoutManagerAI: LinearlayoutAI.Factory

    @Inject
    lateinit var adapter: HomeAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setListeners()
    }

    private fun setListeners() {
    }

    private fun initViews() {
        recycler_view.layoutManager = layoutManagerAI.create(context!!)
        recycler_view.adapter = adapter
        adapter.updateList(arrayListOf("1", "2", "3", "4"))
    }
}
