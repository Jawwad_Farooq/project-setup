package com.onebyte.setup.services.repositories

import android.content.SharedPreferences
import com.onebyte.setup.services.networkServices.NetworkService
import retrofit2.Response
import javax.inject.Inject

class Repository @Inject constructor(private val networkService: NetworkService, private val sharedPreferences: SharedPreferences) {
    suspend fun getList(): Response<String> {
        return networkService.getList()
    }
}