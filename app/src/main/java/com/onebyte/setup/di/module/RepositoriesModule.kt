package com.onebyte.setup.di.module

import android.content.SharedPreferences
import com.onebyte.setup.services.networkServices.NetworkService
import com.onebyte.setup.services.repositories.Repository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
class RepositoriesModule {


}